package rita.androidthingstest

import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.LifecycleObserver
import android.arch.lifecycle.OnLifecycleEvent
import com.google.android.things.contrib.driver.apa102.Apa102
import com.google.android.things.contrib.driver.rainbowhat.RainbowHat

class MainBoardComponents : LifecycleObserver {


    private lateinit var ledstrip: Apa102

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    fun onCreate() {
        ledstrip = RainbowHat.openLedStrip()
        ledstrip.brightness = 1
    }


    fun setLights(lights: RainbowLightsState) {
        if (lights.state){
            lights.setRainbowSpectrum()
            ledstrip.write(lights.spectrumRGB)
        }
        else ledstrip.brightness=0
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun onDestroy() {
        ledstrip.close()
    }
}