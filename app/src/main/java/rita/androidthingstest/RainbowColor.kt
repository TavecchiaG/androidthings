package rita.androidthingstest

enum class RainbowColor(val colorValue: Int) {
    RED(16711680),
    YELLOW(16767744),
    GREEN(4849408),
    CYAN(65426),
    BLUE(37631),
    PURPLE(4784383),
    PINK(16711899)
}