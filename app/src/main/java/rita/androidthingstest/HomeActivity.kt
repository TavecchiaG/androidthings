package rita.androidthingstest

import android.app.Activity
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.bluetooth.BluetoothClass
import android.content.Context
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.TextView
import com.google.android.things.pio.Gpio
import com.google.android.things.pio.GpioCallback
import com.google.android.things.pio.PeripheralManager
import com.google.android.things.contrib.driver.rainbowhat.RainbowHat;
import com.google.android.things.contrib.driver.ht16k33.Ht16k33
import com.google.android.things.contrib.driver.ht16k33.AlphanumericDisplay
import com.google.android.things.contrib.driver.pwmspeaker.Speaker
import com.google.android.things.contrib.driver.bmx280.Bmx280
import android.hardware.SensorManager
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.Sensor.TYPE_AMBIENT_TEMPERATURE
import android.content.Context.SENSOR_SERVICE
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.hardware.Sensor
import android.support.v4.app.FragmentActivity
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.nearby.Nearby
import com.google.android.gms.nearby.connection.*
import com.google.android.gms.nearby.messages.Message
import com.google.android.gms.nearby.messages.MessageListener
import com.google.android.gms.nearby.messages.Strategy
import com.google.android.gms.nearby.messages.SubscribeOptions
import com.google.android.things.contrib.driver.bmx280.Bmx280SensorDriver
import com.google.android.things.contrib.driver.button.Button.OnButtonEventListener
import java.io.IOException
import com.google.android.things.contrib.driver.apa102.Apa102
import com.google.firebase.FirebaseApp
import com.google.firebase.database.*
import com.mikhaellopez.circularprogressbar.CircularProgressBar
import java.io.InputStream
import java.lang.Exception
import java.math.RoundingMode
import java.net.URL
import java.text.DecimalFormat
import java.time.temporal.Temporal
import kotlin.math.abs
import kotlin.math.truncate


/**
 * Skeleton of an Android Things activity.
 *
 * Android Things peripheral APIs are accessible through the class
 * PeripheralManagerService. For example, the snippet below will open a GPIO pin and
 * set it to HIGH:
 *
 * <pre>{@code
 * val service = PeripheralManagerService()
 * val mLedGpio = service.openGpio("BCM6")
 * mLedGpio.setDirection(Gpio.DIRECTION_OUT_INITIALLY_LOW)
 * mLedGpio.value = true
 * }</pre>
 * <p>
 * For more complex peripherals, look for an existing user-space driver, or implement one if none
 * is available.
 *
 * @see <a href="https://github.com/androidthings/contrib-drivers#readme">https://github.com/androidthings/contrib-drivers#readme</a>
 *
 */
class HomeActivity : FragmentActivity(), SensorEventListener {

     lateinit var incomingPayloads: MutableMap<String, Payload>


    val TAG = "HomeActivity"
    val BUTTON_PIN = "BCM21"

    var measureSelected : Int = 0 // 0: temperature on Board, 1:pressure
    var previousTemp : Double = 0.0
    var previousPressure : Double = 0.0

    lateinit var sensorDriver : Bmx280SensorDriver

    //Leds
    lateinit var ledRed: Gpio
    lateinit var ledBlue: Gpio
    lateinit var ledGreen: Gpio

    lateinit var ledstrip : Apa102
    lateinit var rainbow : IntArray
    var mpressed : Boolean = false

    lateinit var myThread : Thread


    lateinit var segment : AlphanumericDisplay

    //A,B,C Buttons
    lateinit var buttonA: com.google.android.things.contrib.driver.button.Button
    lateinit var buttonB : com.google.android.things.contrib.driver.button.Button
    lateinit var buttonC : com.google.android.things.contrib.driver.button.Button

    //Grafica
    lateinit var gButtonA : Button
    lateinit var tempBoardTV: TextView
    lateinit var pressBoardTV: TextView
    lateinit var imageWeb : ImageView
    lateinit var circularProgressBarTemp : CircularProgressBar
    lateinit var nearbyTV : TextView
    lateinit var messageTV : TextView

    /*lateinit var boardComponents:MainBoardComponents
    lateinit var viewModel: MainViewModel
    lateinit var viewModelFactory :ViewModelProvider.Factory*/


    val mCallback = object : GpioCallback{
        override fun onGpioEdge(gpio: Gpio?): Boolean {
            try {
                Log.d("TAG","GPIO Changed Button ${gpio?.value}")
                if (gpio != null) {
                    gButtonA.setText("${if(gpio.value)"premuto" else "non premuto"}")
                    //mLedGpio.value=gpio.value
                }
            }catch (e: NullPointerException){
                Log.d("TAG","ERROR")
            }
            return true
        }

    }

    override fun onCreate(savedInstanceState: Bundle?) {

        incomingPayloads = mutableMapOf()

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        gButtonA = findViewById(R.id.buttonA)
        tempBoardTV = findViewById(R.id.tempBoardTV)
        pressBoardTV = findViewById(R.id.pressBoardTV)
        circularProgressBarTemp = findViewById(R.id.circularProgressBarTemp)

        var animationDuration = 2500; // 2500ms = 2,5s
        circularProgressBarTemp.setProgressWithAnimation(65f , animationDuration); // Default duration = 1500ms


        val pioManager: PeripheralManager = PeripheralManager.getInstance()
        val textView = findViewById<TextView>(R.id.textView)
        nearbyTV = findViewById(R.id.nearbyTV)
        messageTV = findViewById(R.id.messageTV)

        imageWeb = findViewById(R.id.imageView)


        /*
            val inputStream = URL("http://192.168.10.248:8080/shot.jpg").content as InputStream

*/




        textView.setText("Available GPIO :) : ${pioManager.gpioList} ${pioManager.gpioList.size}")

        segment = RainbowHat.openDisplay()

        //Button A
        buttonA = RainbowHat.openButtonA()
        buttonA.setOnButtonEventListener(object : OnButtonEventListener {
            override fun onButtonEvent(button: com.google.android.things.contrib.driver.button.Button?, pressed: Boolean) {
                gButtonA.setText("${if(pressed)"premuto" else "non premuto"}")
                ledRed.value = !ledRed.value
                segment.clear()
                measureSelected = 0
                previousTemp = 0.0
                //segment.display(sensor.readTemperature().toDouble())
            }
        })

        var thread : Thread

        //Button B closes app
        buttonB = RainbowHat.openButtonB()
        buttonB.setOnButtonEventListener(object : OnButtonEventListener {
            override fun onButtonEvent(button: com.google.android.things.contrib.driver.button.Button?, pressed: Boolean) {
                ledGreen.value = !ledGreen.value
                //finish


                thread = Thread(Runnable {
                    Glide.get(applicationContext).clearDiskCache()
                    Log.d("Thread","eccomi")
                })

                thread.start()
                Glide.with(applicationContext).load("http://192.168.10.248:8080/shot.jpg").diskCacheStrategy(DiskCacheStrategy.NONE)
                        .skipMemoryCache(true).into(imageWeb)


                //slideArray(rainbow)


            }
        })


        //Button C
        buttonC = RainbowHat.openButtonC()
        buttonC.setOnButtonEventListener(object: OnButtonEventListener{
            override fun onButtonEvent(button: com.google.android.things.contrib.driver.button.Button?, pressed: Boolean) {
                ledBlue.value = !ledBlue.value
                segment.clear()
                measureSelected = 1
                previousPressure = 0.0
                //segment.display(sensor.readPressure().toDouble())
            }

        })

        //Leds
        ledRed = RainbowHat.openLedRed()
        ledBlue = RainbowHat.openLedBlue()
        ledGreen = RainbowHat.openLedGreen()

        reset()

        //RAINBOW
        ledstrip = RainbowHat.openLedStrip()
        /*
        rainbow = IntArray(RainbowHat.LEDSTRIP_LENGTH)
        for (i in rainbow.indices) {
            rainbow[i] = Color.HSVToColor(0, floatArrayOf(i * 360f / rainbow.size, 1.0f, 1.0f))
        }

        ledstrip.write(rainbow)*/


        /*viewModel=ViewModelProviders.of(this).get(MainViewModel::class.java)

        viewModel.lightsLiveData.observe(this, Observer { lights -> lights?.let {
            boardComponents.setLights(it)
        }})*/


        var assistantDatabase: FirebaseDatabase = FirebaseDatabase.getInstance()
        var lightsData: DatabaseReference = assistantDatabase.getReference("lights")

        lightsData.addValueEventListener(object:ValueEventListener{
            override fun onCancelled(p0: DatabaseError) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onDataChange(p0: DataSnapshot) {
                val state = p0.child("on").value as Boolean
                var lights: RainbowLightsState = RainbowLightsState(state)
                Log.i("Firebase","FUNZIONA")
                if (lights.state) {
                    lights.setRainbowSpectrum()
                    ledstrip.brightness = 1
                    ledstrip.write(lights.spectrumRGB)
                    slideArray(lights.spectrumRGB)
                } else {
                    slideArray(lights.spectrumRGB)
                    ledstrip.write(IntArray(lights.spectrumRGB.size){0})
                }
            }

        })




        //Display
        segment.setBrightness(Ht16k33.HT16K33_BRIGHTNESS_MAX)
        segment.setEnabled(true)
        segment.display("CIAO")


        initSensorDriver(getI2CBus())
        try {
            Thread.sleep(100)
        }catch (e: Exception){
            Log.d("Debug","Errore in sleep")
        }



        startReadingTemperatureAndPressure()


        //START ADVERTISING
        val mPayloadCallback : PayloadCallback = object : PayloadCallback(){
            override fun onPayloadReceived(endPoint: String, payload: Payload) {
                //add payload to map
                incomingPayloads[endPoint] = payload
            }

            override fun onPayloadTransferUpdate(endPoint: String, update: PayloadTransferUpdate) {
                when(update.status){
                    PayloadTransferUpdate.Status.IN_PROGRESS ->{
                        var size = update.totalBytes
                        if (size.equals(-1)){
                            //i'm receiving a stream
                        }
                    }
                    PayloadTransferUpdate.Status.SUCCESS ->{
                        //Transferred 100%
                        val message = incomingPayloads.remove(endPoint)?.asBytes()
                        val string = String(message!!)
                        //${incomingPayloads.remove(endPoint)?.asBytes()?.let { "errore nel parsing" }}
                        nearbyTV.text = "Nearby API: Received payload ${string} from ${endPoint}"
                    }
                    PayloadTransferUpdate.Status.FAILURE -> {
                        //ERROR
                    }
                }
            }
        }

        val mConnectionLifeCycleCallback : ConnectionLifecycleCallback = object: ConnectionLifecycleCallback (){
            override fun onConnectionInitiated(endPointID: String, connectionInfo: ConnectionInfo) {
                Nearby.getConnectionsClient(applicationContext).acceptConnection(endPointID, mPayloadCallback);
            }

            override fun onConnectionResult(endPointID: String, result: ConnectionResolution) {
                when (result.status.statusCode){
                    ConnectionsStatusCodes.STATUS_OK -> {
                        //connected!
                        Log.d("Things","connected")
                        nearbyTV.text ="Nearby API: Connected to ${endPointID}"

                    }
                    ConnectionsStatusCodes.STATUS_CONNECTION_REJECTED ->Log.d("Things","Connection rejected")
                        //Connection rejected
                    ConnectionsStatusCodes.STATUS_ERROR -> Log.d("Things","Connection error")
                }
             }
            override fun onDisconnected(endPointID: String) {
                // We've been disconnected from this endpoint. No more data can be
                // sent or received.
                nearbyTV.text = "Nearby API: disconnected from ${endPointID}"
            }
        }

        Nearby.getConnectionsClient(this).startAdvertising(
                "SERVICE_ID",
                "SERVICE_ID",
                mConnectionLifeCycleCallback,
                AdvertisingOptions.Builder().setStrategy(com.google.android.gms.nearby.connection.Strategy.P2P_CLUSTER).build()).addOnSuccessListener ({ void ->
                    //ADvertising!
                    segment.display("ok!")
                    nearbyTV.text = "Nearby API: Success advertising..."
                    Log.d("Advert","success advertising")
            }).addOnFailureListener({
                    //error
                    segment.display(":(")
                    nearbyTV.text = "Nearby API: Error during advertising"
                    Log.d("Advert","error advertising")

            })

        //Nearby Message

        /*var messageListener=object: MessageListener(){
            override fun onFound(message: Message?) {
                super.onFound(message)
                var stringa=String(message!!.content)
                Log.d("Rasp", stringa)
                messageTV.append("\nReceived: ${stringa}")

            }

            override fun onLost(message: Message?) {
                super.onLost(message)
            }
        }

        var options = SubscribeOptions.Builder()
            .setStrategy(Strategy.BLE_ONLY)
            .build();
        Nearby.getMessagesClient(this).subscribe(messageListener,options)*/


    }


    private fun slideArray(array: IntArray){
        var count = 0
        while(count < array.size){
            val temp = array.get(array.size-1)
            for(i in array.indices.reversed()){
                if(i!=0)
                    array[i] = array[i-1]
            }
            array[0] = temp
            ledstrip.write(array)

            try {
                Thread.sleep(100)
            }catch (e: Exception){
                Log.d("Debug","Errore in sleep")
            }
            count++
        }

    }

    private fun getI2CBus() : String{
        val peripheralManager = PeripheralManager.getInstance()
        val deviceList = peripheralManager.i2cBusList
        if(!deviceList.isEmpty())
            return deviceList[0]
        return "I2C1"
    }


    private fun initSensorDriver(i2cPort: String) {
        sensorDriver = Bmx280SensorDriver(i2cPort)
        // Register the peripheral with the system as a temperature sensor.
        sensorDriver.registerTemperatureSensor()
        sensorDriver.registerPressureSensor()
    }

    private fun startReadingTemperatureAndPressure() {
        val sensorManager = getSystemService(Context.SENSOR_SERVICE) as SensorManager
        // Find the Sensor that we registered.
        val temperatureSensor = sensorManager.getDynamicSensorList(Sensor.TYPE_AMBIENT_TEMPERATURE)[0]
        val pressureSensor = sensorManager.getDynamicSensorList(Sensor.TYPE_PRESSURE)[0]


        sensorManager.registerListener(this, temperatureSensor, SensorManager.SENSOR_DELAY_NORMAL)
        sensorManager.registerListener(this, pressureSensor, SensorManager.SENSOR_DELAY_NORMAL)

    }

    private fun closeSensorDriver() {
        val sensorManager = getSystemService(Context.SENSOR_SERVICE) as SensorManager
        sensorManager.unregisterListener(this)
        // This unregisters the peripheral as a temperature sensor.
        sensorDriver.close()
    }

    // from SensorEventListener interface
    override fun onSensorChanged(event: SensorEvent) {
        when(event.sensor.type){
            Sensor.TYPE_AMBIENT_TEMPERATURE -> {
                if(abs(event.values[0] - previousTemp) > 0.03){
                    previousTemp = event.values[0].toDouble()

                    val df = DecimalFormat("#.##")
                    df.roundingMode = RoundingMode.CEILING
                    tempBoardTV.setText(df.format(previousTemp))
                    circularProgressBarTemp.progress = previousTemp.toFloat()

                    Log.i("TEMP", "Temperature changed: ${event.values[0]}")

                    if(measureSelected == 0)
                        segment.display(df.format(previousTemp))
                }
            }
            Sensor.TYPE_PRESSURE -> {
                if(abs(event.values[0] - previousPressure) > 0.08){
                    previousPressure = event.values[0].toDouble()

                    val df = DecimalFormat("#.##")
                    df.roundingMode = RoundingMode.CEILING
                    pressBoardTV.setText(df.format(previousPressure))

                    Log.i("PRESS", "Pressure changed: ${event.values[0]}")

                    if(measureSelected ==1 )
                        segment.display(df.format(previousPressure))
                }
            }
        }


    }

    override fun onAccuracyChanged(sensor: Sensor, accuracy: Int) {
        Log.i("TEMP", "Sensor accuracy changed: $accuracy")
    }

    fun reset(){
        segment.clear()
        ledRed.value = false
        ledBlue.value = false
        ledGreen.value = false
    }

    override fun onDestroy() {
        super.onDestroy()
        reset()

        //Close button
        buttonA.close()
        buttonB.close()
        buttonC.close()

        segment.close()

        ledRed.close()
        ledBlue.close()
        ledGreen.close()

        closeSensorDriver()
        Nearby.getConnectionsClient(this).stopAdvertising()
        Nearby.getConnectionsClient(this).stopAllEndpoints()
    }




    /*//GPIO BUTTON INPUT
  mButtonGpio = pioManager.openGpio(BUTTON_PIN)
  mButtonGpio.setDirection(Gpio.DIRECTION_IN)
  mButtonGpio.setEdgeTriggerType(Gpio.EDGE_BOTH)
  mButtonGpio.setActiveType(Gpio.ACTIVE_LOW)

  /*mLedGpio = pioManager.openGpio(LED_PIN)
  // Configure as an output.
  mLedGpio.setDirection(Gpio.DIRECTION_OUT_INITIALLY_LOW)*/
  mButtonGpio.registerGpioCallback(mCallback)
*/

    /*sensor = RainbowHat.openSensor()

        sensor.temperatureOversampling = Bmx280.OVERSAMPLING_1X
        textView.setText(sensor.readTemperature().toString())

        sensor.pressureOversampling = Bmx280.OVERSAMPLING_1X
        textView3.setText(sensor.readPressure().toString())*/
}
