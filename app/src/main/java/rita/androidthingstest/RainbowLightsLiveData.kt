package rita.androidthingstest

import android.arch.lifecycle.LiveData
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.ValueEventListener

class RainbowLightsLiveData (val firebase: DatabaseReference) : LiveData<RainbowLightsState>(){

    private val valueEventListener = object : ValueEventListener {
        override fun onDataChange(snapshot: DataSnapshot) {
            val isOn = snapshot.child("on").getValue(Boolean::class.java)
            if(isOn!=null)
                value = RainbowLightsState(isOn)
        }

        override fun onCancelled(error: DatabaseError) { /*handle*/}
    }

    override fun onActive() {
        firebase.child("rainbowlights").addValueEventListener(valueEventListener)
    }

    override fun onInactive() {
        firebase.child("rainbowlights").removeEventListener(valueEventListener)
    }


}


