package rita.androidthingstest

import android.graphics.Color
import android.util.Log
import com.google.android.things.contrib.driver.rainbowhat.RainbowHat

class RainbowLightsState(var state:Boolean ) {



    lateinit var spectrumRGB: IntArray

    init {
        setRainbowSpectrum()
    }

    public fun setRainbowSpectrum(){
        var rainbow = IntArray(RainbowHat.LEDSTRIP_LENGTH)
        Log.d("Valori", rainbow.size.toString());
        for (i in rainbow.indices) {
            rainbow[i] = Color.HSVToColor(0, floatArrayOf(i * 360f / rainbow.size, 1.0f, 1.0f))
            Log.d("Valori 2",rainbow[i].toString())
        }

        spectrumRGB=rainbow
    }



}
