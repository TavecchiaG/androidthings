package com.example.iotivity_encapsulation

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.example.iotivity_encapsulation.server.MainActivity

class StartActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_start)
        //Server
        val intent = Intent(this, MainActivity::class.java)
        //Client
        //val intent = Intent(this, com.example.iotivity_encapsulation.client.MainActivity::class.java)

        startActivity(intent)

    }
}
