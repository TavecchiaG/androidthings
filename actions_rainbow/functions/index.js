// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });

'use strict';

const functions = require('firebase-functions');
const {smarthome} = require('actions-on-google');
const util = require('util');
const admin = require('firebase-admin');
// Initialize Firebase
admin.initializeApp();
const firebaseRef = admin.database().ref('lights');

const app = smarthome({
  debug: true,
  key: 'AIzaSyA5zjDl_qRaRlfHkI5Xig0OCyCPylYSLqE',
});

exports.smarthome = functions.https.onRequest(app);

app.onSync((body) => {
  return {
    requestId: body.requestId,
    payload: {
      agentUserId: '123',
      devices: [{
        id: 'Arcobaleno',
        type: 'action.devices.types.LIGHT',
        traits: [
          'action.devices.traits.OnOff',
          'action.devices.commands.ColorAbsolute'
        ],
        name: {
          defaultNames: ['Arcobaleno'],
          name: 'Arcobaleno',
          nicknames: ['Arcobaleno'],
        },
        deviceInfo: {
          manufacturer: 'RiTa',
          model: 'Deluxe',
          hwVersion: '1.0',
          swVersion: '1.0.1',
        },
      }],
    },
  };
});

app.onQuery((body) => {
  const {requestId} = body;
  const payload = {
    devices: {},
  };
  const queryPromises = [];
  for (const input of body.inputs) {
    for (const device of input.payload.devices) {
      const deviceId = device.id;
      queryPromises.push(queryDevice(deviceId)
        .then((data) => {
          // Add response to device payload
          payload.devices[deviceId] = data;
          return true;
        }
        ));
    }
  }
  // Wait for all promises to resolve
  return Promise.all(queryPromises).then((values) => ({
    requestId: requestId,
    payload: payload,
  })
  );
});

app.onExecute((body) => {
  const {requestId} = body;
  const payload = {
    commands: [{
      ids: [],
      status: 'SUCCESS',
      states: {
        online: true,
      },
    }],
  };
  for (const input of body.inputs) {
    for (const command of input.payload.commands) {
      for (const device of command.devices) {
        const deviceId = device.id;
        payload.commands[0].ids.push(deviceId);
        for (const execution of command.execution) {
          const execCommand = execution.command;
          const {params} = execution;
          switch (execCommand) {
            case 'action.devices.commands.OnOff':
              firebaseRef.update({
                on: params.on
              });
              payload.commands[0].states.on = params.on;
              break;
            case 'action.devices.commands.ColorAbsolute':
            	firebaseRef.update({
            		spectrumRGB: params.spectrumRGB
            	});
            	payload.commands[0].states.spectrumRGB = params.spectrumRGB;
            	break;


              /*

            case 'action.devices.commands.StartStop':
              firebaseRef.child(deviceId).child('StartStop').update({
                isRunning: params.start,
              });
              payload.commands[0].states.isRunning = params.start;
              break;
            case 'action.devices.commands.PauseUnpause':
              firebaseRef.child(deviceId).child('StartStop').update({
                isPaused: params.pause,
              });
              payload.commands[0].states.isPaused = params.pause;
              break;
              */
          }
        }
      }
    }
  }
  return {
    requestId: requestId,
    payload: payload,
  };
});


exports.requestsync = functions.https.onRequest((request, response) => {
  console.info('Request SYNC for user 123');
  app.requestSync('123')
    .then((res) => {
      console.log('Request sync completed');
      response.json(data);
      return true; //AGGIUNTO
    }).catch((err) => {
      console.error(err);
    });
});

/**
 * Send a REPORT STATE call to the homegraph when data for any device id
 * has been changed.
 */
exports.reportstate = functions.database.ref('{deviceId}').onWrite((event) => {
  console.info('Firebase write event triggered this cloud function');
});




exports.fakeauth = functions.https.onRequest((request, response) => {
  const responseurl = util.format('%s?code=%s&state=%s',
    decodeURIComponent(request.query.redirect_uri), 'xxxxxx',
    request.query.state);
  console.log(responseurl);
  return response.redirect(responseurl);
});

exports.faketoken = functions.https.onRequest((request, response) => {
  const grantType = request.query.grant_type
    ? request.query.grant_type : request.body.grant_type;
  const secondsInDay = 86400; // 60 * 60 * 24
  const HTTP_STATUS_OK = 200;
  console.log(`Grant type ${grantType}`);

  let obj;
  if (grantType === 'authorization_code') {
    obj = {
      token_type: 'bearer',
      access_token: '123access',
      refresh_token: '123refresh',
      expires_in: secondsInDay,
    };
  } else if (grantType === 'refresh_token') {
    obj = {
      token_type: 'bearer',
      access_token: '123access',
      expires_in: secondsInDay,
    };
  }
  response.status(HTTP_STATUS_OK)
    .json(obj);
});

const queryFirebase = (deviceId) => firebaseRef.once('value')
  .then((snapshot) => {
    const snapshotVal = snapshot.val();
    return {
      on: snapshotVal.on,
      spectrumRGB: snapshotVal.spectrumRGB
    };
  });

// eslint-disable-next-line
const queryDevice = (deviceId) => queryFirebase(deviceId).then((data) => ({
  on: data.on,
  spectrumRGB: data.spectrumRGB
}));