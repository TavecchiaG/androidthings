package g.tavecchia.companionapp

import android.Manifest
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.TextView
import com.google.android.gms.nearby.Nearby
import android.app.PendingIntent
import android.content.Context
import android.content.pm.PackageManager
import android.support.v4.content.ContextCompat
import android.widget.EditText
import android.widget.Toast
import com.google.android.gms.nearby.connection.*
import com.google.android.gms.nearby.messages.Message
import com.google.android.gms.nearby.messages.MessageListener
import com.google.android.gms.nearby.messages.SubscribeOptions
import android.content.Intent
import android.text.Editable
import com.google.android.gms.nearby.messages.Strategy.BLE_ONLY






class MainActivity : AppCompatActivity() {


    lateinit var incomingPayloads: MutableMap<String, Payload>
    lateinit var mPayloadCallback: PayloadCallback
    lateinit var mConnectionLifeCycleCallback: ConnectionLifecycleCallback

    lateinit var discoveryButton: Button
    lateinit var advertisingTV: TextView
    lateinit var discoveryTV: TextView
    lateinit var payloadTV: TextView
    lateinit var messageConversation: TextView
    lateinit var messageText: EditText
    lateinit var buttonMessage: Button

    private val REQUIRED_PERMISSIONS = arrayOf<String>(Manifest.permission.BLUETOOTH, Manifest.permission.BLUETOOTH_ADMIN, Manifest.permission.ACCESS_WIFI_STATE, Manifest.permission.CHANGE_WIFI_STATE, Manifest.permission.ACCESS_COARSE_LOCATION)

    private val REQUEST_CODE_REQUIRED_PERMISSIONS = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        discoveryButton = findViewById(R.id.discoveryButton)
        discoveryTV = findViewById(R.id.discoveryText)
        advertisingTV = findViewById(R.id.advertising_text)
        payloadTV = findViewById(R.id.payload_text)

        messageText= findViewById(R.id.message_text)
        buttonMessage= findViewById(R.id.message_button)

        messageConversation= findViewById(R.id.message_conversation)

        messageText.setOnClickListener{view ->
            messageText.setText("")
        }


        var messageListener=object:MessageListener(){

            override fun onFound(message: Message?) {
                super.onFound(message)
                var stringa=String(message!!.content)
                messageConversation.append("\nReceived: ${stringa}")
            }

            override fun onLost(message: Message?) {
                super.onLost(message)
            }
        }

        /*var options = SubscribeOptions.Builder()
                .setStrategy(com.google.android.gms.nearby.messages.Strategy.DEFAULT)
                .build();
        Nearby.getMessagesClient(this).subscribe(messageListener,options)*/
        backgroundSubscribe()


        buttonMessage.setOnClickListener{view ->
            var text:String =messageText.text.toString()
            var message=Message(text.toByteArray(Charsets.UTF_8))
            Nearby.getMessagesClient(this@MainActivity).publish(message)
            messageConversation.append("\nSent: ${text}")
            messageText.setText("")
        }


        incomingPayloads = mutableMapOf()
        //Create connection and payload callback
        //DISCOVERY PAAYLOAD CALLBACK
        mPayloadCallback = object : PayloadCallback(){
            override fun onPayloadReceived(endPoint: String, payload: Payload) {
                //add payload to map
                incomingPayloads[endPoint] = payload
            }

            override fun onPayloadTransferUpdate(endPoint: String, update: PayloadTransferUpdate) {
                when(update.status){
                    PayloadTransferUpdate.Status.IN_PROGRESS ->{
                        var size = update.totalBytes
                        if (size.equals(-1)){
                            //i'm receiving a stream
                        }
                    }
                    PayloadTransferUpdate.Status.SUCCESS ->{
                        //Transferred 100%
                        val message = incomingPayloads.remove(endPoint)?.asBytes()
                        if(message!=null){
                            val string = String(message)
                            payloadTV.append( "Nearby API: Received payload ${string} from ${endPoint}")
                        }
                        //${incomingPayloads.remove(endPoint)?.asBytes()?.let { "errore nel parsing" }}
                    }
                    PayloadTransferUpdate.Status.FAILURE -> {
                        //ERROR
                    }
                }
            }
        }


        mConnectionLifeCycleCallback  = object: ConnectionLifecycleCallback (){
            override fun onConnectionInitiated(endPointID: String, connectionInfo: ConnectionInfo) {
                Nearby.getConnectionsClient(applicationContext).acceptConnection(endPointID, mPayloadCallback);
            }

            override fun onConnectionResult(endPointID: String, result: ConnectionResolution) {
                when (result.status.statusCode){
                    ConnectionsStatusCodes.STATUS_OK -> {
                        payloadTV.text="Connected to ${endPointID}"
                        //Connected and send payload
                        Nearby.getConnectionsClient(this@MainActivity).sendPayload(endPointID, Payload.fromBytes("Messaggio inviato da smartphone".toByteArray()))
                    }
                    ConnectionsStatusCodes.STATUS_CONNECTION_REJECTED ->payloadTV.text="Connessione rifiutata"
                    //Connection rejected
                    ConnectionsStatusCodes.STATUS_ERROR -> payloadTV.text="Errore di connessione"
                }
            }
            override fun onDisconnected(endPointID: String) {
                // We've been disconnected from this endpoint. No more data can be
                // sent or received.
            }
        }


        discoveryButton.setOnClickListener{view ->
            discoveryTV.text = "avvio discovery"
            Nearby.getConnectionsClient(this).stopDiscovery()
            startDiscovery()
        }


        if (!hasPermissions(this, REQUIRED_PERMISSIONS.contentDeepToString())) {
            requestPermissions(REQUIRED_PERMISSIONS, REQUEST_CODE_REQUIRED_PERMISSIONS);
        }

        startAdvertising()
    }



    private fun startDiscovery(){
        val mEndpointDiscoveryCallback = object : EndpointDiscoveryCallback() {
            override fun onEndpointFound(
                    endpointId: String, discoveredEndpointInfo: DiscoveredEndpointInfo) {
                // An endpoint was found!
                discoveryTV.text = "Found endpoint: ${endpointId}"
                Nearby.getConnectionsClient(this@MainActivity).requestConnection(
                    "Smartphone",
                    endpointId,
                    mConnectionLifeCycleCallback)
                    .addOnSuccessListener {
                        // We successfully requested a connection. Now both sides
                        // must accept before the connection is established.
                    }.addOnFailureListener({
                            Log.d("Companion", "Error during connection")
                        }) }

            override fun onEndpointLost(endpointId: String) {
                // A previously discovered endpoint has gone away.
            }
        }

        Nearby.getConnectionsClient(this).startDiscovery(
                "SERVICE_ID",
                mEndpointDiscoveryCallback,
                DiscoveryOptions.Builder().setStrategy(Strategy.P2P_CLUSTER).build())
                .addOnSuccessListener ({ void ->
                    discoveryTV.text = "starting discovery"
                })
                .addOnFailureListener(){void ->
                    discoveryTV.text = "Error discovery: Something went wrong during discovery"
                }
    }



    private fun startAdvertising(){
        //START ADVERTISING
        Nearby.getConnectionsClient(this).stopAdvertising()
        Nearby.getConnectionsClient(this).startAdvertising(
                "SERVICE_ID",
                "SERVICE_ID",
                mConnectionLifeCycleCallback,
                AdvertisingOptions.Builder().setStrategy(Strategy.P2P_CLUSTER).build()).addOnSuccessListener ({ void ->
            //ADvertising!
            advertisingTV.text="start advertising"
        }).addOnFailureListener({ void ->
            //error
            advertisingTV.text="advertising error"
        })


    }

    // Subscribe to messages in the background.
    private fun backgroundSubscribe() {
        Log.i("NearbyActivity", "Subscribing for background updates.")
        val options = SubscribeOptions.Builder()
                .setStrategy(BLE_ONLY)
                .build()
        Nearby.getMessagesClient(this).subscribe(getPendingIntent(), options)
    }

    private fun getPendingIntent(): PendingIntent {
        return PendingIntent.getBroadcast(this, 0, Intent(this, BeaconMessageReceiver::class.java),
                PendingIntent.FLAG_UPDATE_CURRENT)
    }

    /** Returns true if the app was granted all the permissions. Otherwise, returns false.  */
    private fun hasPermissions(context: Context, vararg permissions: String): Boolean {
        for (permission in permissions) {
            if (ContextCompat.checkSelfPermission(context, permission) !== PackageManager.PERMISSION_GRANTED) {
                return false
            }
        }
        return true
    }

    override fun onRequestPermissionsResult(
            requestCode: Int,  permissions: Array<String>,  grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (requestCode != REQUEST_CODE_REQUIRED_PERMISSIONS) {
            return
        }

        for (grantResult in grantResults) {
            if (grantResult == PackageManager.PERMISSION_DENIED) {
                Toast.makeText(this, "Missing permissions", Toast.LENGTH_LONG).show()
                finish()
                return
            }
        }

    }
}
