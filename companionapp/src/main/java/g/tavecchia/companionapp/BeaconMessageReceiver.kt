package g.tavecchia.companionapp

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.provider.AlarmClock.EXTRA_MESSAGE
import android.support.v4.content.ContextCompat.startActivity
import android.util.Log
import com.google.android.gms.nearby.Nearby
import com.google.android.gms.nearby.messages.Message
import com.google.android.gms.nearby.messages.MessageListener


class BeaconMessageReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {
        Nearby.getMessagesClient(context).handleIntent(intent, object : MessageListener() {
            override fun onFound(message: Message) {
                Log.i("BroadcastReceiver", "Found message via PendingIntent: $message")
                var stringa=String(message!!.content)
                System.out.println(stringa)
                val intentYee = Intent(context, MainActivity::class.java).apply {
                    putExtra(EXTRA_MESSAGE, message)
                }
                startActivity(context, intentYee,null)
            }

            override fun onLost(message: Message) {
                Log.i("BroadcastReceiver", "Lost message via PendingIntent: $message")
            }
        })
    }


}