

'use strict';

// Import the Dialogflow module from the Actions on Google client library.
const {dialogflow} = require('actions-on-google');
//process.env.DEBUG = 'actions-on-google:*';
//const Assistant = require('actions-on-google').ApiAiAssistant;
const admin = require('firebase-admin');

// Import the firebase-functions package for deployment.
const functions = require('firebase-functions');

// Instantiate the Dialogflow client.
//const app = dialogflow({debug: true});
const app = dialogflow({debug: true});



admin.initializeApp(functions.config().firebase);
const lightsStatus = admin.database().ref('lights');


// API.AI Intent names
const RAINBOW_ON_INTENT = 'rainbowON';
const RAINBOW_OFF_INTENT = 'rainbowOFF';

app.intent(RAINBOW_ON_INTENT, conv => {
	    lightsStatus.update({ on: true });
});


app.intent(RAINBOW_OFF_INTENT, conv => {
	    lightsStatus.update({ on: false });
});


exports.rainbow = functions.https.onRequest(app);


exports.fakeauth = functions.https.onRequest((request, response) => {
  const responseurl = util.format('%s?code=%s&state=%s',
    decodeURIComponent(request.query.redirect_uri), 'xxxxxx',
    request.query.state);
  console.log(responseurl);
  return response.redirect(responseurl);
});

exports.faketoken = functions.https.onRequest((request, response) => {
  const grantType = request.query.grant_type
    ? request.query.grant_type : request.body.grant_type;
  const secondsInDay = 86400; // 60 * 60 * 24
  const HTTP_STATUS_OK = 200;
  console.log(`Grant type ${grantType}`);

  let obj;
  if (grantType === 'authorization_code') {
    obj = {
      token_type: 'bearer',
      access_token: '123access',
      refresh_token: '123refresh',
      expires_in: secondsInDay,
    };
  } else if (grantType === 'refresh_token') {
    obj = {
      token_type: 'bearer',
      access_token: '123access',
      expires_in: secondsInDay,
    };
  }
  response.status(HTTP_STATUS_OK)
    .json(obj);
});


// Import the appropriate service
const { smarthome } = require('actions-on-google');

// Create an app instance
let jwt;
try {
  jwt = require('./key.json');
} catch (e) {
  console.warn('Service account key is not found');
  console.warn('Report state will be unavailable');
}

const smart = smarthome({
  debug: true,
  key: 'AIzaSyA5zjDl_qRaRlfHkI5Xig0OCyCPylYSLqE',
  jwt: jwt,
});

// Register handlers for Smart Home intents
smart.onSync((body) => {
  return {
    requestId: body.requestId,
    payload: {
      agentUserId: '123456789',
      devices: [{
        id: 'rainbow',
        type: 'action.devices.types.LIGHT',
        traits: [
          'action.devices.traits.OnOff'
        ],
        name: {
          defaultNames: ['Super Arcobaleno'],
          name: 'Super Arcobaleno',
          nicknames: ['Arcobaleno'],
        },
        deviceInfo: {
          manufacturer: 'RiVa Enterprise',
          model: 'Super Arcobaleno Deluxe Edition',
          hwVersion: '1.0',
          swVersion: '1.0.1',
        },
        attributes: {
          pausable: true,
          availableModes: [{
            name: 'load',
            name_values: [{
              name_synonym: ['load'],
              lang: 'en',
            }],
            settings: [{
              setting_name: 'small',
              setting_values: [{
                setting_synonym: ['small'],
                lang: 'en',
              }],
            }, {
              setting_name: 'large',
              setting_values: [{
                setting_synonym: ['large'],
                lang: 'en',
              }],
            }],
            ordered: true,
          }],
          availableToggles: [{
            name: 'Turbo',
            name_values: [{
              name_synonym: ['turbo'],
              lang: 'en',
            }],
          }],
        },
      }],
    },
  };
});

