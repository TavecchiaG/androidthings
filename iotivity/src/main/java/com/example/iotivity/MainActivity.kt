package com.example.iotivity

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.example.iotivity.client.*
import com.example.iotivity.server.DeviceDiscoveryServer
import com.example.iotivity.server.GroupServer
import com.example.iotivity.server.PresenceServer
import com.example.iotivity.server.SimpleServer
import com.example.iotivity.server.fridge.FridgeServer
import com.example.iotivity.server.fridge_group.FridgeGroupServer

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val intent = Intent(this, GroupServer::class.java)

        startActivity(intent)    }
}
