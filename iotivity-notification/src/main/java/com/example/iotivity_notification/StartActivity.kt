package com.example.iotivity_notification

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.example.iotivity_notification.provider.MainActivity

class StartActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_start)

        //provider
        val intent = Intent(this, MainActivity::class.java)

        //consumer
        //val intent = Intent(this, com.example.iotivity_notification.consumer.MainActivity::class.java)


        startActivity(intent)    }

}
